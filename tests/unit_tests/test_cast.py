import unittest
from unittest.mock import Mock, patch

from fw_hpc_client.cast import main


class TestCast(unittest.TestCase):
    @patch("fw_hpc_client.cast.os")
    @patch("fw_hpc_client.cast.frame")
    def test_main(self, mock_frame, mock_os):
        mock_os.getcwd.return_value = "/path/to/current/directory"
        mock_frame.timer.return_value = "start_time"
        mock_frame.log = "log"
        mock_frame.run_cmd.return_value = "config"

        with patch("fw_hpc_client.cast.run_cast") as mock_run_cast:
            main()

        mock_os.getcwd.assert_called_once()
        mock_frame.timer.assert_called_once()
        mock_frame.run_cmd.assert_called_once_with()
        mock_run_cast.assert_called_once_with("start_time", "config", "log")

    @patch("fw_hpc_client.cast.os")
    @patch("fw_hpc_client.cast.frame")
    def test_main_keyboard_interrupt(self, mock_frame, mock_os):
        mock_os.getcwd.return_value = "/path/to/current/directory"
        mock_frame.timer.return_value = "start_time"
        mock_frame.log = Mock()
        mock_frame.run_cmd.return_value = "config"

        with patch("fw_hpc_client.cast.run_cast") as mock_run_cast:
            mock_run_cast.side_effect = KeyboardInterrupt
            main()

        mock_os.getcwd.assert_called_once()
        mock_frame.timer.assert_called_once()
        mock_frame.run_cmd.assert_called_once_with()
        mock_frame.log.error.assert_called_once_with("Aborted by Ctrl-C")


if __name__ == "__main__":
    unittest.main()
