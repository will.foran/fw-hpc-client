import argparse
import os
from copy import deepcopy
from datetime import datetime
from pathlib import Path
from unittest.mock import Mock, patch

import pytest
import requests.exceptions

from fw_hpc_client.util.defn import Config, CredentialEnv, Paths
from fw_hpc_client.util.frame import (
    check_paths,
    cmd_parser,
    create_client,
    elapsed_ms,
    fatal,
    fw_fatal,
    load_env_settings,
    load_yaml_settings,
    prepare_config,
    pretty_json,
    run_cmd,
    timer,
)
from tests.assets.variables import CAST, CREDS


@patch("fw_hpc_client.util.frame.log")
@patch("sys.exit")
def test_fatal(mock_exit, mock_log):
    """Test the fatal logging function.

    Args:
        mock_exit (Mock): Mock of the sys.exit function.
        mock_log (Mock): Mock of util.frame.log variable.
    """
    error_message = "An error occurred."
    # Mock the log.critical method
    mock_log.critical = Mock()

    # Call the function
    fatal(error_message)

    # Assert that log.critical was called with the correct arguments
    assert mock_log.critical.call_count == 2
    assert error_message in mock_log.critical.call_args_list[0][0]
    mock_log.critical.assert_called_with("Exiting.")

    # Assert that sys.exit was called with the correct argument
    mock_exit.assert_called_once_with(1)


@patch("fw_hpc_client.util.frame.log")
@patch("fw_hpc_client.util.frame.fatal")
def test_fw_fatal(mock_fatal, mock_log):
    """Test the fw_fatal logging function.

    Args:
        mock_fatal (_type_): _description_
        mock_log (_type_): _description_
    """
    error_message = "Test Message"
    # Mock the log.critical method
    mock_log.critical = Mock()

    # Mock the error object
    error_obj = Exception("Test error")

    # Call the function
    fw_fatal(error_message, error_obj)

    # Assert that log.critical was called with the correct message
    mock_log.critical.assert_called_with(f"{error_message} HTTP error follows:")

    # Assert that fatal was called with the error object
    mock_fatal.assert_called_once_with(error_obj)


def test_pretty_json():
    """Test the pretty_json function."""
    # Case 1: Use a Simple Dictionary
    # Define a sample dictionary
    obj = {"name": "John Doe", "age": 30, "city": "New York"}

    # Call the function
    result = pretty_json(obj)

    # Define the expected output
    expected_output = """{
    "age": 30,
    "city": "New York",
    "name": "John Doe"
}"""

    # Assert the result matches the expected output
    assert result == expected_output

    # Case 2: Use a Dictionary with a PosixPath
    # Define a sample dictionary
    obj = {"name": "John Doe", "age": 30, "city": "New York", "path": Path("/test")}

    # Call the function
    result = pretty_json(obj)

    # Define the expected output
    expected_output = """{
    "age": 30,
    "city": "New York",
    "name": "John Doe",
    "path": "/test"
}"""

    # Assert the result matches the expected output
    assert result == expected_output


@patch("fw_hpc_client.util.frame.datetime")
def test_timer(mock_datetime):
    """Test the function of the timer function.

    Args:
        mock_datetime (Mock): Mock of the datetime class.
    """
    # Mock the current date and time
    mock_now = datetime(2022, 1, 1, 12, 0, 0)
    mock_datetime.now = Mock()
    mock_datetime.now.return_value = mock_now

    # Call the function
    result = timer()

    # Assert the result matches the mocked date and time
    assert result == mock_now


@patch("fw_hpc_client.util.frame.datetime")
def test_elapsed_ms(mock_datetime):
    """Test the elapsed_ms function.

    Args:
        mock_datetime (Mock): Mock of the datetime class.
    """
    # Mock the current date and time
    mock_now = datetime(2022, 1, 1, 12, 0, 0)
    mock_datetime.now = Mock(return_value=mock_now)

    # Define the start time
    start_time = datetime(2022, 1, 1, 11, 59, 0)

    # Call the function
    result = elapsed_ms(start_time)

    # Calculate the expected elapsed time in milliseconds
    expected_elapsed = (mock_now - start_time).total_seconds() * 1000

    # Assert the result matches the expected elapsed time
    assert result == int(expected_elapsed)


@patch("fw_hpc_client.util.frame.fatal")
def test_check_paths(mock_fatal):
    """Test the check_paths function.

    Args:
        mock_fatal (Mock): Mock of the fatal function.
    """
    base_folder = Path("/base_folder")

    # Mock the existence of the paths
    os.path.exists = Mock(return_value=True)

    # Call the function
    result = check_paths(base_folder)

    # Assert the return value is of type Paths
    assert isinstance(result, Paths)

    # Assert the paths are set correctly
    assert result.cast_path == base_folder
    assert result.yaml_path == base_folder / "settings/cast.yml"
    assert result.scripts_path == base_folder / "logs/generated"
    assert result.hpc_logs_path == base_folder / "logs/queue"
    assert result.engine_run_path == base_folder / "logs/temp"

    # Assert that os.path.exists was called for each path
    assert os.path.exists.call_count == 5

    # Assert that fatal was not called
    assert not mock_fatal.called


@patch("fw_hpc_client.util.frame.fatal")
def test_check_paths_missing_paths(mock_fatal):
    """Test the check_paths function when paths are missing.

    Args:
        mock_fatal (Mock): Mock of the fatal function.
    """
    base_folder = Path("/base_folder")

    os.path.exists = Mock(return_value=False)

    # Call the function
    _ = check_paths(base_folder)

    # Assert that fatal was called for each missing path
    assert mock_fatal.call_count == 5


@pytest.fixture
def sample_yaml(tmp_path):
    """Function to create a sample yaml file.

    Args:
        tmp_path (Pathlike): The temporary folder path.

    Returns:
        Pathlike: The path to the sample yaml file.
    """
    yaml_content = """
    key1: value1
    key2: value2
    """
    yaml_path = tmp_path / "sample.yaml"
    with open(yaml_path, "w") as f:
        f.write(yaml_content)
    return yaml_path


@patch("fw_hpc_client.util.frame.defn.ConfigFile.parse_obj")
@patch("yaml.full_load")
def test_load_yaml_settings(mock_full_load, mock_parse_obj, sample_yaml):
    """Test loading a sample yaml file.

    Args:
        mock_full_load (Mock): Mock of the yaml.full_load function.
        mock_parse_obj (Mock): Mock of the parse_obj function.
        sample_yaml (fixture): Fixture to create a sample yaml file.
    """
    full_load_result = {"key1": "value1", "key2": "value2"}
    parse_obj_result = {"key1": "mocked_value1", "key2": "mocked_value2"}
    # Mocking the yaml.full_load function
    mock_full_load.return_value = full_load_result

    # Mocking the defn.ConfigFile.parse_obj function
    mock_parse_obj.return_value = parse_obj_result

    # Mocking the open function in the with statement
    result = load_yaml_settings(sample_yaml)

    # Asserting the expected behavior
    mock_full_load.assert_called_once()
    mock_parse_obj.assert_called_once_with(full_load_result)
    assert result == parse_obj_result


def test_load_env_settings():
    test_host = "example.com"
    test_port = "8080"
    test_secret = "super_secret"

    """Test creating and loading environment variables."""
    os.environ["SCITRAN_RUNTIME_HOST"] = test_host
    os.environ["SCITRAN_RUNTIME_PORT"] = test_port
    os.environ["SCITRAN_CORE_DRONE_SECRET"] = test_secret

    # Call the function
    result = load_env_settings()

    # Assert the result is of type CredentialEnv
    assert isinstance(result, CredentialEnv)

    # Assert the host, port, and credential are set correctly
    assert result.host == test_host
    assert result.port == int(test_port)
    assert result.credential == test_secret

    # Clean up the environment variables
    del os.environ["SCITRAN_RUNTIME_HOST"]
    del os.environ["SCITRAN_RUNTIME_PORT"]
    del os.environ["SCITRAN_CORE_DRONE_SECRET"]


@patch("fw_hpc_client.util.frame.defn.Config")
@patch("fw_hpc_client.util.frame.check_paths")
@patch("fw_hpc_client.util.frame.load_yaml_settings")
@patch("fw_hpc_client.util.frame.load_env_settings")
def test_prepare_config(
    mock_load_env_settings, mock_load_yaml_settings, mock_check_paths, mock_config
):
    """Test the prepare_config function.

    Args:
        mock_load_env_settings (Mock): Mock of the load_env_settings function.
        mock_load_yaml_settings (Mock): Mock of the load_yaml_settings function.
        mock_check_paths (Mock): Mock of the check_paths function.
        mock_config (Mock): Mock of the Config class.
    """

    base_folder = Path("/base_folder")
    paths = Paths(
        cast_path=base_folder,
        yaml_path=base_folder / "settings/cast.yml",
        scripts_path=base_folder / "logs/generated",
        hpc_logs_path=base_folder / "logs/queue",
        engine_run_path=base_folder / "logs/temp",
    )

    cast = deepcopy(CAST)

    creds = deepcopy(CREDS)

    # Mock the return values of check_paths, load_yaml_settings, and load_env_settings
    mock_check_paths.return_value = paths
    mock_load_yaml_settings.return_value = Mock()
    mock_load_yaml_settings.return_value.cast = cast
    mock_load_env_settings.return_value = creds

    # Define the input arguments
    args = Mock(folder="/base_folder")

    # Call the function
    _ = prepare_config(args)

    # Assert the return value is of type Config
    mock_config.assert_called_once_with(cast=cast, paths=paths, creds=creds)

    # Assert that check_paths, load_yaml_settings, and load_env_settings were called with the correct arguments
    mock_check_paths.assert_called_once_with("/base_folder")
    mock_load_yaml_settings.assert_called_once_with(base_folder / "settings/cast.yml")
    mock_load_env_settings.assert_called_once()


@patch("fw_hpc_client.util.frame.flywheel.drone_login.create_drone_client")
@patch("fw_hpc_client.util.frame.fw_fatal")
@patch("fw_hpc_client.util.frame.elapsed_ms")
@patch("fw_hpc_client.util.frame.timer")
@patch("fw_hpc_client.util.frame.log")
def test_create_client(
    mock_log, mock_timer, mock_elapsed_ms, mock_fw_fatal, mock_create_drone_client
):
    """Test the create_client function.

    Args:
        mock_log (Mock): Mock of the util.frame.log variable.
        mock_timer (Mock): Mock of the timer function.
        mock_elapsed_ms (Mock): Mock of the elapsed_ms function.
        mock_fw_fatal (Mock): Mock of the fw_fatal function.
        mock_create_drone_client (Mock): Mock of the flywheel.drone_login.create_drone_client function.
    """
    # Mock the log.info method
    mock_log.info = Mock()

    # Mock the timer
    mock_timer.return_value = "mocked_timer"

    # Mock the elapsed_ms
    mock_elapsed_ms.return_value = "mocked_elapsed_ms"

    # Mock the create_drone_client function
    mock_create_drone_client.return_value = "mocked_client"

    # Define the input arguments
    creds = CREDS

    # Call the function
    result = create_client(creds)

    # Assert that log.info was called with the correct message
    mock_log.info.assert_called_once_with("Connecting to FW...")

    # Assert that timer was called
    mock_timer.assert_called_once()

    # Assert that create_drone_client was called with the correct arguments
    mock_create_drone_client.assert_called_once_with(
        creds.host, creds.credential, "python", "hpc queue", port=creds.port
    )

    # Assert that elapsed_ms was called with the correct argument
    mock_elapsed_ms.assert_called_once_with("mocked_timer")

    # Assert that fw_fatal was not called
    assert not mock_fw_fatal.called

    # Assert the result matches the mocked client
    assert result == "mocked_client"


@patch("os.getcwd")
def test_cmd_parser(mock_cwd):
    """Test the cmd_parser function.

    Args:
        mock_cwd (Mock): Mock of the os.getcwd function.
    """
    test_folder = Path("/current_folder/test_folder")
    # Mock the os.getcwd function
    mock_cwd.return_value = test_folder

    # Call the function
    result = cmd_parser()

    # Assert the result is of type ArgumentParser
    assert isinstance(result, argparse.ArgumentParser)

    # Assert the description is set correctly
    assert result.description == "Cast Flywheel jobs onto --> HPC"

    # Assert the default_folder is set correctly
    assert result.get_default("folder") == test_folder

    # Assert the arguments are added correctly
    assert len(result._actions) == 4

    # Assert the first argument is added correctly
    assert result._actions[0].dest == "help"
    assert result._actions[0].type is None
    assert result._actions[0].default == "==SUPPRESS=="
    assert result._actions[0].help == "show this help message and exit"

    # Assert the second argument is added correctly
    assert result._actions[1].dest == "command"
    assert result._actions[1].type == str
    assert result._actions[1].default == "run"
    assert result._actions[1].help == "Command to run (run, setup, reset)"

    # Assert the third argument is added correctly
    assert result._actions[2].dest == "folder"
    assert result._actions[2].type == str
    assert result._actions[2].default == Path("/current_folder/test_folder")
    assert result._actions[2].help == "Run, setup, or reset in a specific folder"

    # Assert the forth argument is added correctly
    assert result._actions[3].dest == "show_config"
    assert result._actions[3].const is True
    assert result._actions[3].help == "JSON export: all configs"


@patch("fw_hpc_client.util.frame.reset_directories")
@patch("fw_hpc_client.util.frame.setup_directories")
@patch("fw_hpc_client.util.frame.sys")
@patch("fw_hpc_client.util.frame.print")
@patch("fw_hpc_client.util.frame.log")
@patch("fw_hpc_client.util.frame.cmd_parser")
@patch("fw_hpc_client.util.frame.prepare_config")
@patch("fw_hpc_client.util.frame.create_client")
def test_run_cmd(
    mock_create_client,
    mock_prepare_config,
    mock_parser,
    mock_log,
    mock_print,
    mock_sys,
    mock_setup_directories,
    mock_reset_directories,
):
    """Test the run_cmd function.

    Args:
        mock_create_client (Mock): Mock of the create_client function.
        mock_prepare_config (Mock): Mock of the prepare_config function.
        mock_parser (Mock): Mock of the cmd_parser function.
        mock_log (Mock): Mock of the util.frame.log variable.
        mock_print (Mock): Mock of the util.frame.print function.
        mock_sys (Mock): Mock of the sys module.
    """
    # Mock the cmd_parser method
    mock_parser.return_value = Mock()

    base_folder = Path("/base_folder")
    paths = Paths(
        cast_path=base_folder,
        yaml_path=base_folder / "settings/cast.yml",
        scripts_path=base_folder / "logs/generated",
        hpc_logs_path=base_folder / "logs/queue",
        engine_run_path=base_folder / "logs/temp",
    )

    cast = deepcopy(CAST)

    creds = CREDS

    # Mock the prepare_config method
    mock_config = Config(cast=cast, paths=paths, creds=creds)
    mock_prepare_config.return_value = mock_config

    # Mock the create_client method
    mock_sdk = Mock()
    mock_create_client.return_value = mock_sdk

    # Case 1: default command is "run", show_config is False
    mock_parser.return_value.parse_args.return_value = argparse.Namespace(
        command="run", folder=base_folder, show_config=False
    )
    # Call the function
    result = run_cmd()

    # Assert that cmd_parser was called
    mock_parser.assert_called_once()

    # Assert that prepare_config was called with the correct arguments
    mock_prepare_config.assert_called_once_with(
        mock_parser.return_value.parse_args.return_value
    )

    # Assert that create_client was called with the correct argument
    mock_create_client.assert_called_once_with(mock_config.creds)

    # Assert that the sdk attribute of the config object was set correctly
    assert mock_config.sdk == mock_sdk

    # Assert that sys.exit was not called
    assert not mock_sys.exit.called

    # Assert the return value is the expected config object
    assert result == mock_config

    # Case 2: command is "setup"
    # Reset the mock objects
    mock_create_client.reset_mock()
    mock_prepare_config.reset_mock()
    mock_parser.reset_mock()
    mock_log.reset_mock()
    mock_print.reset_mock()
    mock_sys.reset_mock()

    mock_parser.return_value.parse_args.return_value = argparse.Namespace(
        command="setup", folder=base_folder, show_config=False
    )

    # Call the function
    result = run_cmd()

    mock_setup_directories.assert_called_once_with(base_folder)
    mock_reset_directories.assert_not_called()

    # Case 3: command is "reset"
    # Reset the mock objects
    mock_create_client.reset_mock()
    mock_prepare_config.reset_mock()
    mock_parser.reset_mock()
    mock_log.reset_mock()
    mock_print.reset_mock()
    mock_sys.reset_mock()
    mock_setup_directories.reset_mock()

    mock_parser.return_value.parse_args.return_value = argparse.Namespace(
        command="reset", folder=base_folder, show_config=False
    )

    # Call the function
    result = run_cmd()

    mock_setup_directories.assert_not_called()
    mock_reset_directories.assert_called_once_with(base_folder)

    # Case 3: show_config is True
    # Reset the mock objects
    mock_create_client.reset_mock()
    mock_prepare_config.reset_mock()
    mock_parser.reset_mock()
    mock_log.reset_mock()
    mock_print.reset_mock()
    mock_sys.reset_mock()

    mock_parser.return_value.parse_args.return_value = argparse.Namespace(
        command="run", folder=base_folder, show_config=True
    )

    # Call the function
    result = run_cmd()

    # Assert that cmd_parser was called
    mock_parser.assert_called_once()

    # Assert that prepare_config was called with the correct arguments
    mock_prepare_config.assert_called_once_with(
        mock_parser.return_value.parse_args.return_value
    )

    # Assert that sys.exit was called
    mock_sys.exit.assert_called_once_with(0)


@patch("fw_hpc_client.util.frame.flywheel.drone_login.create_drone_client")
@patch("fw_hpc_client.util.frame.fw_fatal")
@patch("fw_hpc_client.util.frame.elapsed_ms")
@patch("fw_hpc_client.util.frame.timer")
@patch("fw_hpc_client.util.frame.log")
def test_create_client_exception(
    mock_log, mock_timer, mock_elapsed_ms, mock_fw_fatal, mock_create_drone_client
):
    """Test the create_client function when an exception is raised.

    Args:
        mock_log (Mock): Mock of the util.frame.log variable.
        mock_timer (Mock): Mock of the timer function.
        mock_elapsed_ms (Mock): Mock of the elapsed_ms function.
        mock_fw_fatal (Mock): Mock of the fw_fatal function.
        mock_create_drone_client (Mock): Mock of the flywheel.drone_login.create_drone_client function.
    """
    # Mock the log.info method
    mock_log.info = Mock()

    # Mock the timer
    mock_timer.return_value = "mocked_timer"

    # Mock the elapsed_ms
    mock_elapsed_ms.return_value = "mocked_elapsed_ms"

    # Mock the fw_fatal function
    mock_fw_fatal.return_value = None

    # Mock the flywheel.drone_login.create_drone_client function to raise a ConnectionError
    mock_create_drone_client.side_effect = requests.exceptions.ConnectionError(
        "Test error"
    )
    mock_create_drone_client.return_value = None

    # Define the input arguments
    creds = CREDS

    # Call the function
    result = create_client(creds)

    # Assert that log.info was called with the correct message
    mock_log.info.assert_called_with("Connecting to FW...")

    # Assert that flywheel.drone_login.create_drone_client was called with the correct arguments
    mock_create_drone_client.assert_called_with(
        "example.com", "super_secret", "python", "hpc queue", port=8080
    )

    # Assert that fw_fatal was called with the correct error object
    assert mock_fw_fatal.call_args.args[0] == "Could not connect to FW."

    # Assert that elapsed_ms was called
    mock_elapsed_ms.assert_called_once()

    # Assert the return value is None
    assert result is None
