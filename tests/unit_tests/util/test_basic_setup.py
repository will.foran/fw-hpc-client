from pathlib import Path
from tempfile import TemporaryDirectory
from unittest import TestCase, mock

from fw_hpc_client.util.basic_setup import reset_directories, setup_directories


class TestBasicSetup(TestCase):
    @mock.patch("fw_hpc_client.util.basic_setup.log")
    def test_setup_directories(self, mock_log):
        with TemporaryDirectory() as default_folder:
            default_folder = Path(default_folder)

            # Case 1: Settings directory does not exist and has no files
            # Call the function
            setup_directories(default_folder=default_folder)

            # Check if directories are created
            assert default_folder.exists()
            assert (default_folder / "settings").exists()
            assert (default_folder / "settings/credentials.sh").exists()
            assert (default_folder / "settings/cast.yml").exists()
            assert (default_folder / "settings/start-cast.sh").exists()
            assert (default_folder / "logs").exists()
            assert (default_folder / "logs/generated").exists()
            assert (default_folder / "logs/queue").exists()
            assert (default_folder / "logs/temp").exists()
            assert (default_folder / "logs/temp/log.json").exists()
            assert (default_folder / "logs/temp/log.json").is_symlink()

            # Case 2: Settings directory exists and has files
            # Call the function
            setup_directories(default_folder=default_folder)

            # Check if log messages are printed
            assert mock_log.info.call_count == 2
            mock_log.info.assert_any_call(
                "Settings directory already has files exists, skipping initialization."
            )
            mock_log.info.assert_any_call(
                "If you want to reset the settings directory, remove those files or "
                "run `fw-hpc-client reset`."
            )

    def test_reset_directories(self):
        with TemporaryDirectory() as temp_dir:
            # Create temporary directories for testing
            test_folder = Path(temp_dir) / "test_folder"
            settings_dir = test_folder / "settings"
            log_dir = test_folder / "logs"
            settings_dir.mkdir(parents=True, exist_ok=True)
            log_dir.mkdir(parents=True, exist_ok=True)

            # Mock the input function to return "yess"
            with mock.patch("builtins.input", return_value="yes"):
                # Call the function to reset directories
                reset_directories(test_folder)

            # Check if directories are removed
            assert not settings_dir.exists()
            assert not log_dir.exists()

    def test_reset_directories_cancel(self):
        with TemporaryDirectory() as temp_dir:
            # Create temporary directories for testing
            test_folder = Path(temp_dir) / "test_folder"
            settings_dir = test_folder / "settings"
            log_dir = test_folder / "logs"
            settings_dir.mkdir(parents=True, exist_ok=True)
            log_dir.mkdir(parents=True, exist_ok=True)

            # Mock the input function to return "no"
            with mock.patch("builtins.input", return_value="no"):
                # Call the function to reset directories
                reset_directories(test_folder)

            # Check if directories are not removed
            assert settings_dir.exists()
            assert log_dir.exists()
