from copy import deepcopy
from unittest.mock import Mock

import pytest

from fw_hpc_client.cluster.lsf import SCRIPT_TEMPLATE, Lsf
from fw_hpc_client.util.defn import Config
from tests.assets.variables import CAST, CREDS, PATHS, job


class TestLsf:
    @pytest.fixture(autouse=True)
    def setup(self):
        """Setup common variables used by all test member functions."""
        # Config is instantiated for each test to avoid side effects from overwriting
        # the same object from other tests and files.
        self.config = Config(paths=PATHS, cast=deepcopy(CAST), creds=CREDS)
        self.log = Mock()
        self.job = job

        self.lsf = Lsf(self.config, self.log)

    def test_set_config_defaults(self):
        """Test setting the default configuration settings for the Lsf Scheduler."""

        self.lsf.set_config_defaults()
        c = self.lsf.config.cast

        assert c.command == [
            "bsub",
            "-P",
            "flywheel",
            "-J",
            "fw-{{job.fw_id}}",
            "-oo",
            "{{script_log_path}}",
            "-eo",
            "{{script_log_path}}",
        ]
        assert c.command_script_stdin is True
        assert c.script == SCRIPT_TEMPLATE
        assert c.script_executable is False

    def test_determine_job_settings(self):
        """Test determining the job settings for the Lsf Scheduler."""

        # Test Job without GPU
        job_settings = self.lsf.determine_job_settings(self.job)

        assert job_settings.fw_id == str(self.job.id)
        assert job_settings.singularity_debug is False
        assert job_settings.singularity_writable is False
        assert job_settings.ram == CAST.scheduler_ram
        assert job_settings.cpu == CAST.scheduler_cpu
        assert job_settings.gpu is None

    def test_format_scheduler_ram_and_cpu_settings(self):
        """Test formatting the scheduler ram and cpu settings for the Lsf Scheduler."""
        scheduler_ram = "8G"
        scheduler_cpu = "4"

        # Case 1: Test with valid scheduler ram and cpu settings
        ram, cpu = self.lsf.format_scheduler_ram_and_cpu_settings(
            scheduler_ram, scheduler_cpu
        )

        assert ram == scheduler_ram
        assert cpu == scheduler_cpu

        # Case 2: Test with valid scheduler ram and invalid cpu settings
        ram, cpu = self.lsf.format_scheduler_ram_and_cpu_settings(scheduler_ram, None)

        assert ram == scheduler_ram
        assert cpu == self.lsf.default_cpu_count

        # Case 3: Test with invalid scheduler ram and valid cpu settings
        ram, cpu = self.lsf.format_scheduler_ram_and_cpu_settings(None, scheduler_cpu)

        assert ram == self.lsf.default_ram
        assert cpu == scheduler_cpu
