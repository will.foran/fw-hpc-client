from pathlib import Path
from unittest.mock import Mock, patch

import flywheel
import pytest

from fw_hpc_client.cluster.common import Common, net
from fw_hpc_client.cluster.slurm import SCRIPT_TEMPLATE
from fw_hpc_client.util.defn import JobSettings, ScriptTemplate


class TestCommon:
    @pytest.fixture(autouse=True)
    def setup(self, tmp_path):
        """Setup common variables used by all test member functions."""
        self.config = Mock()
        self.config.cast = Mock()
        self.config.cast.dict.return_value = {
            "command": "some_command",
            "command_script_stdin": "some_command_script_stdin",
            "script": "some_script",
            "script_executable": "some_script_executable",
        }
        self.config.cast.filter = "some_filter"
        self.config.cast.cast_on_tag = True
        self.config.cast.filter_tags = None
        self.config.cast.cast_gear_whitelist = []
        self.config.paths.cast_path = "/path/to/cast"
        self.config.paths.engine_run_path = "/path/to/engine_run"
        self.config.sdk = Mock()
        self.log = Mock()
        self.common = Common(self.config, self.log)
        self.common.uid_whitelist = None
        self.common.config.cast.group_whitelist = True

    def test_confirm_config_defaults_loaded(self):
        """Test confirming the default configuration settings are loaded."""
        self.common.confirm_config_defaults_loaded()

        assert self.config.cast.dict.call_count == 4
        self.log.fatal.assert_not_called()

    @patch("fw_hpc_client.cluster.common.frame.fatal")
    def test_confirm_config_defaults_error(self, mock_fatal):
        """Test configuration defaults error.

        Args:
            mock_fatal (Mock): Mock of the frame.fatal function.
        """
        missing_key = "command"
        self.config.cast.dict.return_value[missing_key] = None

        """Test confirming the default configuration settings are loaded."""
        self.common.confirm_config_defaults_loaded()
        mock_fatal.assert_called_once_with(
            f"config.cast. {missing_key} not populated. "
            "Modify your set_config_defaults implementation."
        )

    def test_get_jobs(self):
        """Test getting the jobs from a flywheel instance."""
        self.jobs = [Mock(), Mock()]
        self.config.sdk.jobs.iter_find.return_value = iter(self.jobs)

        result = self.common.get_jobs()

        assert result == self.jobs
        self.config.sdk.jobs.iter_find.assert_called_once_with(
            filter="state=running,tags=hpc"
        )
        self.log.debug.assert_not_called()

    def test_get_jobs_filter_tags(self):
        """Test getting the jobs from a flywheel instance."""
        # Set the filter tags
        self.config.cast.filter_tags = ["tag1", "tag2"]

        # Set up the test data
        self.jobs = [Mock(), Mock()]
        self.jobs[0].tags = ["tag1", "tag2"]
        self.jobs[1].tags = ["tag1", "tag3", "hpc"]

        self.config.sdk.jobs.iter_find.return_value = iter(self.jobs)

        result = self.common.get_jobs()

        assert result == [self.jobs[0]]

        self.config.sdk.jobs.iter_find.assert_called_once_with(
            filter="state=running,tags=hpc"
        )
        self.log.debug.assert_not_called()

        # Unset the filter tags
        self.config.cast.filter_tags = None

    @patch("fw_hpc_client.cluster.common.frame.fw_fatal")
    def test_get_jobs_error(self, mock_fw_fatal):
        """Test error case when fetching FW jobs.

        Args:
            mock_fw_fatal (Mock): Mock of the frame.fw_fatal function.
        """
        self.common.fw.jobs.iter_find.side_effect = flywheel.rest.ApiException("Error")

        self.common.get_jobs()

        mock_fw_fatal.assert_called_once_with(
            "Could not fetch FW jobs.", self.common.fw.jobs.iter_find.side_effect
        )

    def test_determine_singularity_settings(self):
        """Test determining the singularity settings for a job."""
        job = Mock()
        job.config = {"singularity-debug": True, "singularity-writable": False}

        result = self.common.determine_singularity_settings(job)

        assert result == (True, False)
        self.log.warn.assert_not_called()

    def test_determine_singularity_settings_invalid_types(self):
        """Test determining the singularity settings for a job with invalid types."""
        job = Mock()
        job.config = {"singularity-debug": "true", "singularity-writable": 123}

        result = self.common.determine_singularity_settings(job)

        assert result == (False, False)
        self.log.warn.assert_called_with(
            "Invalid singularity-writable type on job. Ignoring."
        )

    @patch("fw_hpc_client.cluster.common.net.load_user_id_whitelist")
    def test_load_whitelist(self, mock_load_user_id_whitelist):
        """Test loading the user whitelist.

        Args:
            mock_load_user_id_whitelist (Mock): Mock of the load_user_id_whitelist function.
        """
        mock_load_user_id_whitelist.return_value = ["user1", "user2"]

        self.common.load_whitelist()

        net.load_user_id_whitelist.assert_called_once_with(self.common.fw)
        assert self.common.uid_whitelist == ["user1", "user2"]
        self.common.log.warn.assert_not_called()

    @patch("fw_hpc_client.cluster.common.frame.fw_fatal")
    @patch("fw_hpc_client.cluster.common.net.load_user_id_whitelist")
    def test_load_whitelist_error(self, mock_load_user_id_whitelist, mock_fw_fatal):
        """Test error case when loading the user whitelist."""
        self.common.uid_whitelist = None
        self.common.config.cast.group_whitelist = True

        mock_load_user_id_whitelist.side_effect = flywheel.rest.ApiException("Error")

        self.common.load_whitelist()

        mock_fw_fatal.assert_called_once_with(
            "Could not fetch HPC whitelist.", mock_load_user_id_whitelist.side_effect
        )
        assert self.common.uid_whitelist is None

    @patch("fw_hpc_client.cluster.common.net.load_user_id_whitelist")
    def test_load_whitelist_empty(self, mock_load_user_id_whitelist):
        """Test loading the user whitelist when it is empty.

        Args:
            mock_load_user_id_whitelist (Mock): Mock of the load_user_id_whitelist function.
        """
        mock_load_user_id_whitelist.return_value = []

        self.common.load_whitelist()

        net.load_user_id_whitelist.assert_called_once_with(self.common.fw)
        assert self.common.uid_whitelist == []
        self.common.log.warn.assert_called_once_with(
            "HPC whitelist is active, but empty! No jobs will run."
        )

    @patch("fw_hpc_client.cluster.common.net.add_system_log")
    @patch("fw_hpc_client.cluster.common.net.cancel_job")
    def test_reject_whitelist(self, mock_cancel_job, mock_add_system_log):
        """Test rejecting a job due to whitelist mismatch.

        Args:
            mock_cancel_job (Mock): Mock of the cancel_job function.
            mock_add_system_log (Mock): Mock of the add_system_log function.
        """
        job = Mock()
        job.id = "job123"
        job_user = "user1"

        mock_add_system_log.return_value = None

        self.config.cast.admin_contact_email = "admin@example.com"

        self.common.reject_whitelist(job, job_user)

        self.log.warn.assert_called_once_with(
            f"User {job_user} is not on the HPC whitelist. Dropping job."
        )

        expected_msg = (
            f"User {job_user} is not on the HPC whitelist.\nOnly white-listed users are "
            "allowed to run Gears on the HPC at this time.\nFor more information "
            f"please contact {self.config.cast.admin_contact_email}"
        )
        mock_add_system_log.assert_called_once_with(
            self.config.sdk, job.id, expected_msg
        )
        mock_cancel_job.assert_called_once_with(self.config.sdk, job.id)
        self.log.debug.assert_called_once()

    @patch("fw_hpc_client.cluster.common.frame.fw_fatal")
    @patch("fw_hpc_client.cluster.common.net.add_system_log")
    @patch("fw_hpc_client.cluster.common.net.cancel_job")
    def test_reject_whitelist_error(
        self, mock_cancel_job, mock_add_system_log, mock_fw_fatal
    ):
        """Test error case when rejecting a job due to whitelist mismatch.

        Args:
            mock_cancel_job (Mock): Mock of the net.cancel_job function.
            mock_add_system_log (Mock): Mock of the net.add_system_log function.
            mock_fw_fatal (Mock): Mock of the frame.fw_fatal function.
        """
        job = Mock()
        job.id = "job123"
        job_user = "user1"

        mock_cancel_job.side_effect = flywheel.rest.ApiException("Error")

        self.config.cast.admin_contact_email = "admin@example.com"

        self.common.reject_whitelist(job, job_user)

        self.log.warn.assert_called_once_with(
            f"User {job_user} is not on the HPC whitelist. Dropping job."
        )

        expected_msg = (
            f"User {job_user} is not on the HPC whitelist.\nOnly white-listed users are "
            "allowed to run Gears on the HPC at this time.\nFor more information "
            f"please contact {self.config.cast.admin_contact_email}"
        )
        mock_add_system_log.assert_called_once_with(
            self.config.sdk, job.id, expected_msg
        )
        mock_cancel_job.assert_called_once_with(self.config.sdk, job.id)
        self.log.debug.assert_called_once()
        mock_fw_fatal.assert_called_once_with(
            "Could not cancel FW job.", mock_cancel_job.side_effect
        )

    @patch("fw_hpc_client.cluster.common.net.load_user_id_whitelist")
    def test_check_whitelist(self, mock_load_user_id_whitelist):
        """Test checking the whitelist for if a job should run.

        Args:
            mock_load_user_id_whitelist (Mock): Mock of the load_user_id_whitelist function.
        """
        job = Mock()
        job.origin = Mock()
        job.origin.type = "user"
        job.origin.id = "user1"
        self.common.reject_whitelist = Mock()

        mock_load_user_id_whitelist.return_value = ["user1", "user2"]

        self.common.load_whitelist()

        result = self.common.check_whitelist(job)

        assert result is True
        self.common.reject_whitelist.assert_not_called()

    @patch("fw_hpc_client.cluster.common.net.load_user_id_whitelist")
    def test_check_whitelist_reject(self, mock_load_user_id_whitelist):
        """Test checking the whitelist for if a job should run and rejecting it.

        Args:
            mock_load_user_id_whitelist (Mock): Mock of the load_user_id_whitelist function.
        """
        job = Mock()
        job.origin = Mock()
        job.origin.type = "user"
        job.origin.id = "user3"

        mock_load_user_id_whitelist.return_value = ["user1", "user2"]
        self.common.reject_whitelist = Mock()
        self.common.load_whitelist()

        result = self.common.check_whitelist(job)

        assert result is False
        self.common.reject_whitelist.assert_called_once_with(job, "user3")

    @patch("os.chmod")
    @patch("os.stat")
    @patch("builtins.open")
    def test_run_templating(self, mock_open, mock_stat, mock_chmod):
        """Test running the templating for a job.

        Args:
            mock_open (Mock): Mock of the open function.
        """
        job = Mock()
        job.id = "job123"
        job_settings = JobSettings(
            fw_id=str(job.id),
            singularity_debug=False,
            singularity_writable=False,
            ram="4G",
            cpu="4",
        )
        values = ScriptTemplate(
            job=job_settings,
            script_path="/path/to/script.sh",
            script_log_path="/path/to/script.log",
            cast_path=self.config.paths.cast_path,
            engine_run_path=self.config.paths.engine_run_path,
        )

        self.config.cast.show_script_template_values = True
        self.config.cast.script = SCRIPT_TEMPLATE
        self.config.cast.show_script_template_result = True
        self.config.cast.script_executable = True
        self.config.cast.command = ["command1", "command2"]
        self.config.cast.dry_run = True
        self.config.cast.show_commnd_template_result = True

        expected_script_text = (
            "#!/bin/bash\n\n"
            "#SBATCH --job-name=fw-job123\n"
            "#SBATCH --ntasks=1\n"
            "#SBATCH --cpus-per-task=4\n"
            "#SBATCH --mem-per-cpu=4G\n"
            "#SBATCH --output /path/to/script.log\n\n"
            "set -euo pipefail\n\n"
            'source "/path/to/cast/settings/credentials.sh"\n'
            'cd "/path/to/engine_run"\n\n'
            "set -x\n"
            "srun ./engine run --single-job job123\n"
        )
        expected_command = ["echo", "command1", "command2"]

        mock_handle = mock_open.return_value
        self.common.log.debug.reset_mock()

        result_script_text, result_command = self.common.run_templating(job, values)

        assert result_script_text == expected_script_text
        assert result_command == expected_command
        mock_open.assert_called_once_with(Path("/path/to/script.sh"), "w")
        mock_handle.write.assert_called_once_with(expected_script_text)
        mock_handle.close.assert_called_once()
        mock_stat.assert_called_once()
        mock_chmod.assert_called_once()

    def test_report_results(self):
        """Test reporting the results of jobs handled."""
        start = 0
        jobs_launched = 5
        jobs_skipped = 2
        jobs_rejected = 1

        with patch("fw_hpc_client.cluster.common.frame.elapsed_ms") as mock_elapsed_ms:
            mock_elapsed_ms.return_value = 100

            self.common.report_results(
                start, jobs_launched, jobs_skipped, jobs_rejected
            )

            mock_elapsed_ms.assert_called_once_with(start)
            self.log.info.assert_called_once_with(
                "Launched 5, rejected 1, skipped 2 jobs. Runtime: 100 ms."
            )

    def test_report_results_no_jobs(self):
        """Test reporting the results of no jobs handled."""
        start = 0
        jobs_launched = 0
        jobs_skipped = 0
        jobs_rejected = 0

        with patch("fw_hpc_client.cluster.common.frame.elapsed_ms") as mock_elapsed_ms:
            mock_elapsed_ms.return_value = 100

            self.common.report_results(
                start, jobs_launched, jobs_skipped, jobs_rejected
            )

            mock_elapsed_ms.assert_called_once_with(start)
            self.log.info.assert_called_once_with("No jobs to handle. Runtime: 100 ms.")
