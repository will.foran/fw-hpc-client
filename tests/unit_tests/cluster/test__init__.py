from copy import deepcopy
from datetime import datetime
from unittest.mock import Mock, patch

import pytest

from fw_hpc_client.cluster import Base, Lsf, Sge, Slurm, from_scheduler, run_cast
from fw_hpc_client.util.defn import Config
from tests.assets.variables import CAST, CREDS, PATHS


class TestRunCast:
    @pytest.fixture(autouse=True)
    def setup(self):
        """Setup common variables used by all test member functions."""

        self.start = datetime.now()
        self.log = Mock()

        # Config is instantiated for each test to avoid side effects from overwriting
        # the same object from other tests and files.
        self.config = Config(paths=PATHS, cast=deepcopy(CAST), creds=CREDS)

    @pytest.mark.parametrize(
        "cluster",
        [
            "base",
            "lsf",
            "sge",
            "slurm",
        ],
    )
    @patch("fw_hpc_client.cluster.Base.handle_all")
    def test_run_cast(self, mock_handle_all, cluster):
        """Test running a cast directly.

        Args:
            mock_handle_all (Mock): The mock of the Base.handle_all function.
            cluster (str): The type of cluster that is used.
        """
        self.config.cast.cluster = cluster
        run_cast(self.start, self.config, self.log)

        # Assert that the correct class's handle_all method is called with the correct arguments
        mock_handle_all.assert_called_with(self.start)

    @patch("fw_hpc_client.cluster.frame.fatal")
    def test_run_cast_invalid_cluster(self, mock_fatal):
        """Test for an invalid cluster type.

        Args:
            mock_fatal (Mock): Mock of the frame.fatal function.
        """
        self.config.cast.cluster = "invalid"

        run_cast(self.start, self.config, self.log)

        # Assert that the frame.fatal method is called with the correct arguments
        mock_fatal.assert_called_with(
            f"No such cluster type: {self.config.cast.cluster}"
        )


class TestFromScheduler:
    @pytest.fixture(autouse=True)
    def setup(self):
        self.start = datetime.now()
        self.log = Mock()

        # Config is instantiated for each test to avoid side effects from overwriting
        # the same object from other tests and files.
        self.config = Config(paths=PATHS, cast=deepcopy(CAST), creds=CREDS)

    @pytest.mark.parametrize(
        "cluster_type, expected",
        [
            ("base", Base),
            ("lsf", Lsf),
            ("sge", Sge),
            ("slurm", Slurm),
        ],
    )
    def test_from_scheduler(self, cluster_type, expected):
        """Test creating a scheduler object from the different scheduler types.

        Args:
            cluster_type (str): Cluster type.
            expected (Base): The type of Cluster Class used.
        """
        scheduler = from_scheduler(self.config, self.log, cluster_type)
        assert isinstance(scheduler, expected)

    @patch("fw_hpc_client.cluster.frame.fatal")
    def test_from_scheduler_invalid_cluster(self, mock_fatal):
        """Test attempting to instantiate an unlisted scheduler type.

        Args:
            mock_fatal (Mock): Mock of the frame.fatal function.
        """
        self.config.cast.cluster = "invalid"

        from_scheduler(self.config, self.log, "invalid")

        # Assert that the sys.exit method is called with the correct arguments
        mock_fatal.assert_called_with(
            f"No such cluster/scheduler type: {self.config.cast.cluster}"
        )
