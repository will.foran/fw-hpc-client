from copy import deepcopy
from datetime import datetime
from unittest.mock import Mock, patch

import pytest
from flywheel import JobListEntry

from fw_hpc_client.cluster.base import SCRIPT_TEMPLATE, Base
from fw_hpc_client.util.defn import Config
from tests.assets.variables import CAST, CREDS, PATHS, job


class TestBase:
    @pytest.fixture(autouse=True)
    def setup(self):
        """Setup common variables used by all test member functions."""
        # Config is instantiated for each test to avoid side effects from overwriting
        # the same object from other tests and files.
        self.config = Config(paths=PATHS, cast=deepcopy(CAST), creds=CREDS)
        self.log = Mock()
        self.job = JobListEntry(**job.dict())
        self.job.reload = Mock()
        self.job.reload.return_value = self.job
        self.base = Base(config=self.config, log=self.log)

    def test_set_config_defaults(self):
        """Test setting configuration defaults.

        Start with certain cast commands at None and ensure they are populated with the
        default values.
        """
        self.base.set_config_defaults()

        assert self.base.config.cast.command == ["echo", "{{script_path}}"]
        assert self.base.config.cast.command_script_stdin is False
        assert self.base.config.cast.script == SCRIPT_TEMPLATE
        assert self.base.config.cast.script_executable is False

    def test_determine_job_settings(self):
        """Test that job settings are populated correctly."""
        # Call the determine_job_settings method
        job_settings = self.base.determine_job_settings(self.job)

        # Assert the expected values
        assert job_settings.fw_id == str(self.job.id)
        assert not job_settings.singularity_debug
        assert job_settings.singularity_writable is False
        assert job_settings.ram is None
        assert job_settings.cpu is None

    def test__determine_scheduler_settings(self):
        """Test the _determine_scheduler_settings method."""
        # Declare temporary variables
        my_scheduler_ram = "16G"
        my_scheduler_cpu = "4"
        # Create a mock job object
        job = Mock()

        # Case 1: The job has scheduler_ram and scheduler_cpu as config options
        job.config = {
            "config": {
                "scheduler_ram": my_scheduler_ram,
                "scheduler_cpu": my_scheduler_cpu,
            }
        }
        job.tags = []
        settings = {"scheduler_ram": None, "scheduler_cpu": None}
        # Call the _determine_scheduler_settings method
        updated_settings = self.base._determine_scheduler_settings(job, settings)

        # Assert the expected values
        assert updated_settings["scheduler_ram"] == my_scheduler_ram
        assert updated_settings["scheduler_cpu"] == my_scheduler_cpu

        # Case 2: The job has scheduler_ram and scheduler_cpu as tags
        settings = {"scheduler_ram": None, "scheduler_cpu": None}
        job.config = {
            "config": {
                "scheduler_ram": None,
                "scheduler_cpu": None,
            }
        }
        job.tags = [
            f"scheduler_ram={my_scheduler_ram}",
            f"scheduler_cpu={my_scheduler_cpu}",
        ]

        # Call the _determine_scheduler_settings method
        updated_settings = self.base._determine_scheduler_settings(job, settings)

        # Assert the expected values
        assert updated_settings["scheduler_ram"] == my_scheduler_ram
        assert updated_settings["scheduler_cpu"] == my_scheduler_cpu

        # Case 3: The job has scheduler_ram as a config option but scheduler_cpu is in
        #         the cast configuration
        my_scheduler_cpu = None
        cast_scheduler_cpu = 5
        job.config = {
            "config": {
                "scheduler_ram": my_scheduler_ram,
                "scheduler_cpu": my_scheduler_cpu,
            }
        }
        job.tags = []
        # Create a mock settings dictionary
        settings = {"scheduler_ram": None, "scheduler_cpu": None}

        # Set the base.config.cast.scheduler_cpu to None to cover the case where the job
        # has a scheduler_ram but no scheduler_cpu
        self.base.config.cast.scheduler_cpu = cast_scheduler_cpu

        # Call the _determine_scheduler_settings method
        updated_settings = self.base._determine_scheduler_settings(job, settings)

        # Assert the expected values
        assert updated_settings["scheduler_ram"] == my_scheduler_ram
        assert updated_settings["scheduler_cpu"] == cast_scheduler_cpu

    def test_determine_script_patch(self):
        """Test the determination of the generated script path."""
        expected_path = self.base.config.paths.scripts_path / f"job-{self.job.id}.sh"
        actual_path = self.base.determine_script_patch(self.job)

        assert actual_path == expected_path

    def test_determine_log_patch(self):
        """Test the generation of the job log path."""
        # Create a mock job object
        job = Mock()
        job.id = "12345"

        # Call the determine_log_patch method
        log_patch = self.base.determine_log_patch(job)

        # Assert the expected log patch
        expected_log_patch = self.base.config.paths.hpc_logs_path / f"job-{job.id}.txt"
        assert log_patch == expected_log_patch

    @patch("subprocess.run")
    @patch("builtins.open")
    @patch("sys.stdout")
    @patch("sys.stderr")
    def test_execute(self, mock_stderr, mock_stdout, mock_open, mock_subprocess_run):
        """Test execution of a sample command and script.

        Args:
            mock_stderr (Mock): Mock of standard error.
            mock_stdout (Mock): Mock of standard output.
            mock_open (Mock): Mock of the builtin open function.
            mock_subprocess_run (Mock): Mock of the subprocess.run function.
        """
        # Set up the test data
        command = ["echo", "Hello, World!"]
        script_path = "/path/to/script.sh"

        # Case 1: self.base.config.cast.command_script_stdin is None
        # Call the execute method
        self.base.execute(command, script_path)

        # Assert the expected calls
        mock_stdout.flush.assert_called_once()
        mock_stderr.flush.assert_called_once()
        mock_subprocess_run.assert_called_once_with(command, check=True)

        # Case 2: self.base.config.cast.command_script_stdin is True
        self.base.config.cast.command_script_stdin = True
        # Reset the mock calls
        mock_stdout.flush.reset_mock()
        mock_stderr.flush.reset_mock()
        mock_subprocess_run.reset_mock()

        # Call the execute method
        self.base.execute(command, script_path)

        # Assert the expected calls
        mock_stdout.flush.assert_called_once()
        mock_stderr.flush.assert_called_once()
        mock_open.assert_called_once()
        mock_subprocess_run.assert_called_once_with(
            command, stdin=mock_open.return_value.__enter__(), check=True
        )

    @patch("fw_hpc_client.cluster.base.Base.execute")
    @patch("fw_hpc_client.cluster.base.Base.run_templating")
    def test_handle_each(self, mock_run_templating, mock_execute):
        """Test the handling of each job.

        Args:
            mock_run_templating (Mock): Mock of templating function.
            mock_execute (Mock): Mock of the execute function.
        """
        # Create a mock job object
        job = Mock()
        job.id = "12345"

        # Create a mock values object
        values = Mock()
        values.script_path = "/path/to/script.sh"

        # Create a mock command
        command = ["echo", "Hello, World!"]
        mock_run_templating.return_value = (values.script_path, command)

        self.base.handle_each(job, values)

        # Assert that run_templating was called with the correct arguments
        mock_run_templating.assert_called_once_with(job, values)

        # Assert that execute was called with the correct arguments
        mock_execute.assert_called_once_with(command, values.script_path)

        # Assert that the log messages were called correctly
        self.log.info.assert_called_once_with("Casting job to HPC...")
        self.log.debug.assert_called()
        assert self.log.debug.call_args_list[0][0] in [
            (f"Casted job in {i} ms.",) for i in range(20)
        ]

    @patch("fw_hpc_client.cluster.base.frame.fatal")
    @patch("fw_hpc_client.cluster.base.Base.execute")
    @patch("fw_hpc_client.cluster.base.Base.run_templating")
    def test_handle_each_error(self, mock_run_templating, mock_execute, mock_fatal):
        """Test the generation of a job handling error.

        Args:
            mock_run_templating (Mock): Mock of the Base.run_templating function.
            mock_execute (Mock): Mock of the Base.execute function.
            mock_fatal (Mock): Mock of the frame.fatal function.
        """
        # Create a mock job object
        job = Mock()
        job.id = "12345"

        mock_execute.side_effect = FileNotFoundError("Script file not found")
        # Create a mock values object
        values = Mock()
        values.script_path = "/path/to/script.sh"

        # Create a mock command
        command = ["echo", "Hello, World!"]
        mock_run_templating.return_value = (values.script_path, command)

        self.base.handle_each(job, values)

        # Assert that run_templating was called with the correct arguments
        mock_run_templating.assert_called_once_with(job, values)

        # Assert that execute was called with the correct arguments
        mock_execute.assert_called_once_with(command, values.script_path)

        # Assert that an error was logged
        mock_fatal.assert_called_once_with(mock_execute.side_effect)

    @patch("os.path.exists")
    @patch("fw_hpc_client.cluster.base.defn.ScriptTemplate")
    @patch("fw_hpc_client.cluster.base.Base.handle_each")
    @patch("fw_hpc_client.cluster.base.Base.determine_job_settings")
    @patch("fw_hpc_client.cluster.base.Base.determine_log_patch")
    @patch("fw_hpc_client.cluster.base.Base.check_whitelist")
    @patch("fw_hpc_client.cluster.base.Base.determine_script_patch")
    @patch("fw_hpc_client.cluster.base.Base.get_jobs")
    @patch("fw_hpc_client.cluster.base.Base.confirm_config_defaults_loaded")
    @patch("fw_hpc_client.cluster.base.Base.set_config_defaults")
    def test_handle_all(
        self,
        mock_set_config_defaults,
        mock_confirm_config_defaults_loaded,
        mock_get_jobs,
        mock_determine_script_patch,
        mock_check_whitelist,
        mock_determine_log_patch,
        mock_determine_job_settings,
        mock_handle_each,
        mock_script_template,
        mock_os_path_exists,
    ):
        """Test handling all jobs that are found on the Flywheel instance.

        Args:
            mock_set_config_defaults (Mock): Mock of the Base.set_config_defaults.
            mock_confirm_config_defaults_loaded (Mock): Mock of Base.confirm_config_defaults_loaded
            mock_get_jobs (Mock): Mock of the Base.get_jobs function.
            mock_determine_script_patch (Mock): Mock of the Base.determine_script_patch function.
            mock_check_whitelist (Mock): Mock of the Base.check_whitelist function.
            mock_determine_log_patch (Mock): Mock of the Base.determine_log_patch function.
            mock_determine_job_settings (Mock): Mock of the Base.determine_job_settings function.
            mock_handle_each (Mock): Mock of the Base.handle_each function.
            mock_script_template (Mock): Mock of the defn.ScriptTemplate class.
            mock_os_path_exists (Mock): Mock of the os.path.exists function.
        """
        # Patch the necessary methods
        mock_get_jobs.return_value = [Mock(), Mock(), Mock()]
        mock_script_template.return_value = Mock()
        path_exists_side_effect = [False, True, False]
        check_whitelist_side_effect = [False, True, True]
        mock_os_path_exists.side_effect = path_exists_side_effect
        mock_check_whitelist.side_effect = check_whitelist_side_effect

        start = datetime.now()
        self.base.handle_all(start=start)

        # Assert that the necessary methods were called with the correct arguments
        mock_set_config_defaults.assert_called_once()
        mock_confirm_config_defaults_loaded.assert_called_once()
        mock_get_jobs.assert_called_once()
        assert mock_determine_script_patch.call_count == len(mock_get_jobs.return_value)
        assert mock_os_path_exists.call_count == len(mock_get_jobs.return_value)
        # we only expect check_whitelist to be called if the corresponding
        # os.path_exists is False
        assert mock_check_whitelist.call_count == path_exists_side_effect.count(False)
        # all other calls should only happen if the path does not exists and the script
        # is whitelisted
        expected_log_job_handle_count = len(
            [
                p
                for p, w in zip(path_exists_side_effect, check_whitelist_side_effect)
                if p is False and w is True
            ]
        )
        assert mock_determine_log_patch.call_count == expected_log_job_handle_count
        assert mock_determine_job_settings.call_count == expected_log_job_handle_count
        assert mock_handle_each.call_count == expected_log_job_handle_count

    def test_check_legacy_ram_and_cpu_settings(self):
        """Test checking legacy RAM and CPU settings.

        Legacy are "slurm-cpu" and "slurm-ram".
        """
        # Create a mock job object
        slurm_ram = "8G"
        slurm_cpu = "8"
        job = Mock()
        job.config = {
            "config": {
                "slurm-ram": slurm_ram,
                "slurm-cpu": slurm_cpu,
            }
        }

        # Call the _check_legacy_ram_and_cpu_settings method
        updated_job = self.base._check_legacy_ram_and_cpu_settings(job)

        # Assert the expected values
        assert updated_job.config["config"]["scheduler_ram"] == slurm_ram
        assert updated_job.config["config"]["scheduler_cpu"] == slurm_cpu
        assert self.base.log.warning.called

        # Reset the mock calls
        self.base.log.warning.reset_mock()
        job.config["config"].pop("slurm-ram")
        job.config["config"].pop("slurm-cpu")
        # Call the _check_legacy_ram_and_cpu_settings method with no legacy settings
        updated_job = self.base._check_legacy_ram_and_cpu_settings(job)

        # Assert the expected values
        assert updated_job.config["config"]["scheduler_ram"] == slurm_ram
        assert updated_job.config["config"]["scheduler_cpu"] == slurm_cpu
        assert not self.base.log.warning.called

    def test_check_legacy_ram_and_cpu_settings_error(self):
        """Test the error condition on the legacy RAM and CPU settings."""
        # Create a mock job object
        slurm_ram = "8G"
        slurm_cpu = "8"
        scheduler_ram = "8G"
        scheduler_cpu = "8"
        job = Mock()
        job.config = {
            "config": {
                "slurm-ram": slurm_ram,
                "slurm-cpu": slurm_cpu,
                "scheduler_ram": scheduler_ram,
                "scheduler_cpu": scheduler_cpu,
            }
        }
        with pytest.raises(ValueError):
            # Call the _check_legacy_ram_and_cpu_settings method
            _ = self.base._check_legacy_ram_and_cpu_settings(job)

    def test_format_scheduler_ram_and_cpu_settings(self):
        """Test the Error condition on formating scheduler and cpu settings.

        This is an abstract function and should raise an error.
        """
        with pytest.raises(NotImplementedError):
            self.base.format_scheduler_ram_and_cpu_settings("8G", "8")

    def test__get_valid_settings_tag_with_valid_tag(self):
        """Test getting the first valid settings tag from a job's tags."""
        # Create a mock job instance
        job_mock = Mock()
        job_mock.tags = ["tag1", "tag2", "cpus=3", "tag3"]

        # Call the function under test
        result = self.base._get_valid_settings_tag(job_mock, "scheduler_cpu")

        # Assert that the result is the valid tag
        assert result == "cpus=3"

    def test__get_valid_settings_tag_with_invalid_tag(self):
        """Test getting the first valid settings tag from a job's tags when no valid tag is found."""
        # Create a mock job instance
        job_mock = Mock()
        job_mock.tags = ["tag1", "tag2", "tag3"]

        # Call the function under test
        result = self.base._get_valid_settings_tag(job_mock, "scheduler_cpu")

        # Assert that the result is None
        assert result is None

    def test__validate_settings_tag_with_scheduler_cpu(self):
        """Test validating a settings tag with 'scheduler_cpu'."""
        tag = "cpus=4"
        settings_tag = "scheduler_cpu"
        result = self.base._validate_settings_tag(tag, settings_tag)
        assert result == "4"

        tag = "cpu=0"
        result = self.base._validate_settings_tag(tag, settings_tag)
        assert result is None

        tag = "cpu=abc"
        result = self.base._validate_settings_tag(tag, settings_tag)
        assert result is None

    def test__validate_settings_tag_with_scheduler_ram(self):
        """Test validating a settings tag with 'scheduler_ram'."""
        tag = "ram-gb=8G"
        settings_tag = "scheduler_ram"
        result = self.base._validate_settings_tag(tag, settings_tag)
        assert result == "8G"

        tag = "ram=8"
        self.log.reset_mock()
        result = self.base._validate_settings_tag(tag, settings_tag)
        assert result == "8G"
        self.log.warning.assert_called_once_with(
            "Assuming Numeric value for RAM is in gigabytes (G)."
        )

        tag = "ram=abc"
        self.log.reset_mock()
        result = self.base._validate_settings_tag(tag, settings_tag)
        assert result is None
        self.log.warning.assert_called_once_with(
            "RAM setting must be in gigabytes (G). Setting to scheduler default."
        )

        tag = "ram=GG"
        self.log.reset_mock()
        result = self.base._validate_settings_tag(tag, settings_tag)
        assert result is None
        self.log.warning.assert_called_once_with(
            "RAM setting must be in gigabytes (G). Setting to scheduler default."
        )

    def test__validate_settings_tag_with_invalid_settings_tag(self):
        """Test validating a settings tag with an invalid settings tag."""
        tag = "some_tag=value"
        settings_tag = "invalid_tag"
        result = self.base._validate_settings_tag(tag, settings_tag)
        assert result is None

    def test_determine_job_priority(self):
        """Test the determination of the job priority."""

        # Case 1: Job with priority "low"
        self.job.priority = "low"
        # Call the determine_job_priority method
        priority = self.base.determine_job_priority(self.job)

        # Assert the expected priority
        assert priority == 0

        # Case 2: Job with priority "medium"
        self.job.priority = "medium"
        # Call the determine_job_priority method
        priority = self.base.determine_job_priority(self.job)

        # Assert the expected priority
        assert priority == 1

        # Case 3: Job with priority "high"
        self.job.priority = "high"
        # Call the determine_job_priority method
        priority = self.base.determine_job_priority(self.job)

        # Assert the expected priority
        assert priority == 2

        # Case 4: Job with priority "critical"
        self.job.priority = "critical"
        # Call the determine_job_priority method
        priority = self.base.determine_job_priority(self.job)

        # Assert the expected priority
        assert priority == 3

        # Case 5: Job without priority mapping
        self.config.cast.map_job_priority = False
        # Call the determine_job_priority method
        priority = self.base.determine_job_priority(self.job)

        # Assert the expected priority
        assert priority is None
