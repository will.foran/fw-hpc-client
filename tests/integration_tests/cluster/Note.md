# Note on Integration Tests

The tests in this directory depend on the `process/setup.sh` script being run.

The integration tests are disabled until they can be implemented on in the CI/CD
framework.
