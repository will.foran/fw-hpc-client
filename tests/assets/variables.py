""" """

from fw_hpc_client.util.defn import (
    Config,
    ConfigFileCast,
    CredentialEnv,
    FlywheelJob,
    Paths,
)

# -----------------------------------------------------------------------------
# Setup the default job for testing
# -----------------------------------------------------------------------------
job_dict = {
    "config": {
        "config": {
            "include_rating_widget": False,
            "measurement": "T1",
            "save_derivatives": False,
            "save_outputs": False,
            "verbose_reports": True,
        },
        "destination": {"id": "64a732ef0ffb0ba95c5c679f", "type": "acquisition"},
        "inputs": {
            "nifti": {
                "base": "file",
                "hierarchy": {"id": "64a732ef0ffb0ba95c5c679f", "type": "acquisition"},
                "location": {
                    "name": "sub-13_T1w.nii.gz",
                    "path": "/flywheel/v0/input/nifti/sub-13_T1w.nii.gz",
                },
                "object": {
                    "classification": {
                        "Features": [],
                        "Intent": [],
                        "Measurement": ["T1"],
                    },
                    "file_id": "64a732fba36314d6133805eb",
                    "mimetype": "application/octet-stream",
                    "modality": "MR",
                    "origin": {"id": "jesusavila@flywheel.io", "type": "user"},
                    "size": 4888210,
                    "tags": [],
                    "type": "nifti",
                    "version": 1,
                    "zip_member_count": None,
                },
            }
        },
    },
    "destination": {"id": "64a732ef0ffb0ba95c5c679f", "type": "acquisition"},
    "gear_id": "64a734a17a38c4247c11ce7a",
    "gear_info": {
        "category": "qa",
        "id": "64a734a17a38c4247c11ce7a",
        "name": "mriqc",
        "version": "0.7.0_0.15.1",
    },
    "id": "64b5bce63a2637356859bf8c",
    "inputs": [
        {
            "base": None,
            "found": None,
            "id": "64a732ef0ffb0ba95c5c679f",
            "input": "nifti",
            "name": "sub-13_T1w.nii.gz",
            "type": "acquisition",
        }
    ],
    "origin": {"id": "jesusavila@flywheel.io", "type": "user"},
    "previous_job_id": None,
    "state": "running",
    "tags": ["hpc", "mriqc"],
    "priority": "high",
}

job = FlywheelJob(**job_dict)

PATHS = Paths(
    cast_path="/base_folder",
    yaml_path="/base_folder/settings/cast.yml",
    scripts_path="/base_folder/logs/generated",
    hpc_logs_path="/base_folder/logs/queue",
    engine_run_path="/base_folder/logs/temp",
)

CAST = ConfigFileCast(
    cluster="mocked_cluster",
    dry_run=False,
    scheduler_ram="8G",
    scheduler_cpu="8",
    admin_contact_email="george.jetson@bed.rock",
    group_whitelist=False,
    cast_on_tag=True,
    cast_gear_whitelist=[],
    show_script_template_values=True,
    show_script_template_result=True,
    show_commnd_template_result=True,
    command=None,
    command_script_stdin=None,
    script=None,
    script_executable=None,
    use_hold_engine=True,
    map_job_priority=True,
    fw_priority_map={"low": 0, "medium": 1, "high": 2, "critical": 3},
)

CREDS = CredentialEnv(host="example.com", port="8080", credential="super_secret")

CONFIG = Config(paths=PATHS, cast=CAST, creds=CREDS)
