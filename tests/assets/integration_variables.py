import argparse
import os
from copy import deepcopy
from pathlib import Path

import flywheel

from fw_hpc_client.util.defn import FlywheelJob
from fw_hpc_client.util.frame import load_yaml_settings, prepare_config

from .variables import job, job_dict

jobs = []
jobs.append(job)

# -----------------------------------------------------------------------------
# Create other jobs for testing other cases and schedulers
# -----------------------------------------------------------------------------
# Create a Flywheel job that has `scheduler_ram` and `scheduler_cpu` variables
# undefined as ''
job_ram_cpu_defined_empty = deepcopy(job_dict)
job_ram_cpu_defined_empty["config"]["config"]["scheduler_ram"] = ""
job_ram_cpu_defined_empty["config"]["config"]["scheduler_cpu"] = ""
job_ram_cpu_defined_empty = FlywheelJob(**job_ram_cpu_defined_empty)
jobs.append(job_ram_cpu_defined_empty)

# Create a Flywheel job that has `scheduler_ram` and `scheduler_cpu` variables
# defined for Slurm
job_ram_cpu_defined_slurm = deepcopy(job_dict)
job_ram_cpu_defined_slurm["config"]["config"]["scheduler_ram"] = "16G"
job_ram_cpu_defined_slurm["config"]["config"]["scheduler_cpu"] = "4"
job_ram_cpu_defined_slurm = FlywheelJob(**job_ram_cpu_defined_slurm)
jobs.append(job_ram_cpu_defined_slurm)

# Create a Flywheel job that has `scheduler_ram` and `scheduler_cpu` variables
# defined for lsf
job_ram_cpu_defined_lsf = deepcopy(job_dict)
job_ram_cpu_defined_lsf["config"]["config"]["scheduler_ram"] = "rusage[mem=5000]"
job_ram_cpu_defined_lsf["config"]["config"]["scheduler_cpu"] = "2"
job_ram_cpu_defined_lsf = FlywheelJob(**job_ram_cpu_defined_lsf)
jobs.append(job_ram_cpu_defined_lsf)

# Create a Flywheel job that has `scheduler_ram` and `scheduler_cpu` variables
# defined for sge
job_ram_cpu_defined_sge = deepcopy(job_dict)
job_ram_cpu_defined_sge["config"]["config"]["scheduler_ram"] = "10G"
job_ram_cpu_defined_sge["config"]["config"]["scheduler_cpu"] = "2-4"
job_ram_cpu_defined_sge = FlywheelJob(**job_ram_cpu_defined_sge)
jobs.append(job_ram_cpu_defined_sge)

# Create a Flywheel job that has `slurm-ram` and `slurm` variables
# undefined as ''
job_slurm_ram_cpu_defined_empty = deepcopy(job_dict)
job_slurm_ram_cpu_defined_empty["config"]["config"]["slurm-ram"] = ""
job_slurm_ram_cpu_defined_empty["config"]["config"]["slurm-cpu"] = ""
job_slurm_ram_cpu_defined_empty = FlywheelJob(**job_slurm_ram_cpu_defined_empty)
jobs.append(job_slurm_ram_cpu_defined_empty)

# Create a Flywheel job that has `slurm-ram` and `slurm-cpu` variables defined
# for slurm
job_slurm_ram_cpu_defined_slurm = deepcopy(job_dict)
job_slurm_ram_cpu_defined_slurm["config"]["config"]["slurm-ram"] = "12G"
job_slurm_ram_cpu_defined_slurm["config"]["config"]["slurm-cpu"] = "6"
job_slurm_ram_cpu_defined_slurm = FlywheelJob(**job_slurm_ram_cpu_defined_slurm)
jobs.append(job_slurm_ram_cpu_defined_slurm)

# -----------------------------------------------------------------------------
# Set up the config and log for the scheduler objects. All these are necessary
# to instantiate a scheduler object (children of Base in
# `src/cluster/base.py`
# -----------------------------------------------------------------------------
# set some env variables
os.environ["SCITRAN_RUNTIME_HOST"] = "latest.sse.flywheel.io"
os.environ["SCITRAN_RUNTIME_PORT"] = "443"
os.environ["SCITRAN_CORE_DRONE_SECRET"] = ""

# TODO: Create a temporary folder with settings and logs... symlink in here
arg_dict = {
    "folder": Path(__file__).parent,
    "show_match": False,
    "show_config": False,
}
args = argparse.Namespace(**arg_dict)
# args = cmd_parser().parse_args()
# Handle running the pytest from Pycharm's debugger or from the command line
script_directory = Path(__file__).parent
# print(script_directory)
if os.path.split(script_directory)[-1] == "assets":
    args.folder = os.path.split(os.path.split(script_directory)[0])[
        0
    ]  # manually set the base folder to the project, 2 folders up
elif os.path.exists(
    os.path.join(script_directory, "tests")
):  # we're already in the project directory
    args.folder = script_directory
else:
    raise ValueError("Cannot determine correct absolute path to main project folder.")
# print("args.folder: %s" % args.folder)


# Create config fucntions to make varieties b/c deepcopy not working
def create_config(args):
    config = prepare_config(args)
    # change cast so that it loads from tests/assets/cast.yml
    config.cast = load_yaml_settings(
        os.path.join(args.folder, "tests/assets/cast.yml")
    ).cast
    config.sdk = flywheel.Client()
    return config


config = create_config(args=args)

# Create config where ram and cpu are empty
config_no_ram_no_cpu = create_config(args=args)
config_no_ram_no_cpu.cast.scheduler_cpu = ""
config_no_ram_no_cpu.cast.scheduler_ram = ""
