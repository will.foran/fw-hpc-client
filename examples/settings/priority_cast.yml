cast:

  # Cluster/Scheduler type.
  # For a list of valid values, see src/cluster/__init__.py.
  cluster: 'slurm'

  # When set, scripts are still generated, but no HPC commands are run.
  # Useful for testing.
  dry_run: false

  # Contact email provided to users who run into errors.
  admin_contact_email: '<Your-Email-Here>'

  # When set, jobs are only allowed from users added to a group whitelist.  If `true`,
  # Cast will look for users added to a group called "hpc-whitelist" in your Flywheel
  # instance. It is recommended that the group label and ID be the same
  # (i.e., "hpc-whitelist")
  group_whitelist: false

  # When set, fw cast will use a user mapping to map posix and flywheel users
  get_specific_user_jobs: true

  # When set, jobs with the tag "hpc" are cast.
  cast_on_tag: true

  # Gear names in this list are always cast.
  # Only works when cast_on_tag is false.
  # Used for backwards compatibility.
  # cast_gear_whitelist:
  #   - safe-python-singularity

  # When enabled, templating properties will be dumped to the log.
  # Useful for debugging a script or command template.
  show_script_template_values: true
  show_script_template_result: true
  show_commnd_template_result: true

  # Internal Flywheel setting. Leave this at default unless instructed.
  use_hold_engine: true

  # When set, the priority of the submitted job is set based on the fw job priority.
  # These are based on Slurm's priority values. The highest priority is 4294967293.
  # See https://slurm.schedmd.com/srun.html#OPT_priority and
  # https://slurm.schedmd.com/priority_multifactor.html for more information.
  map_job_priority: true
  fw_priority_map:
    low: 1
    medium: 2
    high: 3
    critical: 1024

  # Settings for slurm cluster ram and cpu. Uncomment if you wish to set these for all
  # your slurm jobs. Settings these two varables as gear config parameters overrides
  # these, so this allows these to be set for specific jobs. Use the examples in the
  # table below to format for the type of cluster/scheduler.
  #    | scheduler/cluster  | RAM    | CPU    |
  #    | -----------------  | ---    | ---    |
  #    | Slurm              | '8G'   | '8'    |
  #    | LSF                | 'rusage[mem=4000]' | '1' |
  #    | SGE                | '8G' | '4-8'   (sets CPU range) |
  scheduler_ram: '8G'
  scheduler_cpu: '8'

  #
  # Depending on your cluster implementation,
  # the options below may already be set up for you.
  #
  # Check the corresponding python file, and
  # only un-comment these options if the hard-coded defaults
  # need to change for your system.
  #


  # A jinja2-templated array representing the command to execute.
  # See defn.ScriptTemplate for available values.
  # command:
  #   - 'sbatch'
  #   - '{{script_path}}'

  # When set, the script file is marked as executable (chmod +x).
  # script_executable: false

  # When set, the generated script is sent to the HPC command via stdin.
  # command_script_stdin: false

  # A jinja2-templated string representing the script to generate.
  # See defn.ScriptTemplate for available values.
  script: |+
    #!/bin/bash
    #SBATCH --job-name=fw-{{job.fw_id}}
    #SBATCH --ntasks=1
    #SBATCH --cpus-per-task={{job.cpu}}
    #SBATCH --mem-per-cpu={{job.ram}}
    {% if job.priority %}#SBATCH --priority={{job.priority}}{% endif %}
    #SBATCH --output {{script_log_path}}
    set -euo pipefail

    source "{{cast_path}}/settings/credentials.sh"

    export USER=`whoami`

    # singularity working directory for /tmp, /var/tmp, and $HOME
    export SINGULARITY_WORKDIR="/home/$USER/singularity_workdir"
    mkdir -p $SINGULARITY_WORKDIR

    # "SingularityCE will cache SIF container images generated from remote sources,
    # and anyOCI/docker layers used to create them". The default is
    # $HOME/.singularity/cache
    export SINGULARITY_CACHEDIR="/tmp/$USER/singularity_cache"
    mkdir -p $SINGULARITY_CACHEDIR

    cd "{{engine_run_path}}"

    set -x
    srun ./engine run --single-job {{job.fw_id}}
