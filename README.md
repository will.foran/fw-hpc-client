# Flywheel HPC Client <!-- omit in toc -->

The HPC Client is a self-service solution that allows Flywheel jobs and gears to run on
a High Performance Computing environment. Use on-premise hardware that's already
available for highly-concurrent scientific workloads!

## Documentation

The types of HPC schedulers, minimal requirements, setup directions, and answers to
Frequently Asked Questions can be found
[here](https://flywheel-io.gitlab.io/scientific-solutions/app/fw-hpc-client/).
