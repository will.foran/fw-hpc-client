# TODOs

- [ ] Have a direct and guided setup process for each type of deployment
  - [ ] single-user deployment (w or w/o gpu)
    - the configuration directory has exclusive user permissions
  - [ ] multi-user deployment (w or w/o gpu)
    - the configuration directory has group permissions
    - each user has singularity directories with exclusive ownership
    - each user points to the configuration directory on setup
- [ ] Rework logging
  - configure logging in `src/cast.py`
  - each file has its own log that follows the format:

    ```python
      log = logging.getLogger(__name__)
    ```

- [ ] Change `util.defn.ConfigFileCast.cluster` to `util.defn.ConfigFileCast.cluster_type`
  - This will help reduce confusion between cluster/scheduler and cluster_type
    explicitly
- [ ] Change the `src` directory to `hpc_client`
  - [ ] This makes this an PyPi-installable library.
- [ ] Get CI/CD incorporated for automated testing
- [ ] Autoformat all files with pre-commit
  - [ ] ruff
  - [ ] black
  - [ ] isort
