# Using Podman with HPC Client

Using Podman with the HPC Client is simple, but takes some consideration.

## Configuration

### HPC Client Configuration

With the below changes in place, the only thing that needs to change in the HPC Client
configuration is by setting the following line in the  `settings/credentials.sh` file:

```bash
# HPC compatible compute
export ENGINE_MODULE=podman
```

### Software Requirements

As mentioned in the [system requirements](../../system_requirements#podman-executor),
the following software has been tested on Ubuntu 22.04, Jammy Jellyfish:

```bash
$ apt list --installed crun containernetworking-plugins podman-plugins \
podman-docker slirp4netns podman

containernetworking-plugins/jammy,now 0.9.1+ds1-1 amd64 [installed]
crun/unknown,now 101:1.14.4-0ubuntu22.04+obs70.5 amd64 [installed]
podman-docker/unknown,now 4:4.6.2-0ubuntu22.04+obs81.12 all [installed]
podman-plugins/unknown,now 4:4.6.2-0ubuntu22.04+obs81.12 amd64 [installed]
podman/unknown,now 4:4.6.2-0ubuntu22.04+obs81.12 amd64 [installed]
slirp4netns/unknown,now 100:1.1.8-4 amd64 [installed]
```

Ensuring these libraries are installed on the nodes of the Slurm cluster will allow the
`engine` to execute gears. If they are not installed, consult with your system
administrator to facilitate their installation.

#### GPU Requirements

Executing gears on GPU nodes requires the HPC Client and Slurm to be configured to
schedule those nodes (see [Using GPUs on a Slurm Cluster](using_a_gpu_with_slurm.md)).

Furthermore, GPU execution has been successfully performed with the nvidia driver below

```bash
$ apt list --installed nvidia-driver-545

nvidia-driver-545/jammy-updates,now 545.29.06-0ubuntu0.22.04.2 amd64 [installed]
```

#### Engine Requirements

The Flywheel Engine must be updated to enable both Podman execution with or without
GPUs. Please request an engine with these features from Flywheel staff and install
according to the [Updating the Flywheel Compute Engine](updating_the_flywheel_engine.md)
document.
