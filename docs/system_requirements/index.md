# Minimum System Requirements

## RAM: 32 GB

After the HPC Client submits a job to the cluster, Docker containers from Flywheel must
be converted into Singularity containers (SIF files) the first time a gear is run; they
are cached for future runs.

Depending on the gear, this process may require up to 32 GB of RAM (e.g., bids-fmriprep).

This conversion is typically done on compute nodes, but you may want to pre-build
singularity containers on the system (or head/login node) if compute nodes do not have
all singularity tools required for building SIF files.
[Learn more about using prebuilt singularity images](https://docs.flywheel.io/Administrator_Guides/admin_using_prebuilt_singularity_images_with_hpc).

## Local Storage Drive: 64 GB

This assumes that the main storage drive is mapped, and this mapped drive contains all
the cached singularity images.

As mentioned above, the compute notes should have access to this same directory/mapped
drive.

If no mapped drive will be used, then the local storage drive should be sufficiently
large to accommodate the cached singularity files, downloaded files to be processed,
intermediate files when run, etc. ( ~2 TB).

## CPUs: 4

If the system is only submitting jobs to compute nodes, then 4 CPUs should be
sufficient; however, more is recommended (e.g., 16) if you anticipate building SIF files
on the system.

## Operating System: Linux

Common Linux distributions should work with the HPC Client; testing has been done on
Ubuntu, version 20.04.3 LTS (Focal Fossa).

## Software

The versions below have been verified. Unless otherwise instructed, use these versions
or later.

* Singularity: version 3.8.1
* Cron for setting how frequently the HPC Client searches for HPC jobs on Flywheel
* Slurm: slurm-wlm 19.05.5.
* Python 3: version 3.8.10.
* Git: version 2.25.1.

### Podman Executor

Podman may be desired to run gear images without conversion to a singularity image.
Podman has been tested on Ubuntu 22.04, Jammy Jellyfish with the following libraries
installed:

* podman: version 4.6.2
* podman-docker: version 4.6.2
* podman-plugins: version 4.6.2
* crun: version 1.14.4
* slirp4netns: version 1.1.8
* containernetworking-plugins: version 0.9.1
