# hpc-client Command Line Interface

The `fw-hpc-client` is a Python application with commands and flags to manage an
`hpc-client` configuration.

## Basic Commands

Issuing the following command will provide the help for the `hpc-client`:

```bash
$ fw-hpc-client --help

CWD /home/john_doe/code/hpc-client
usage: fw-hpc-client [-h] [--folder FOLDER] [--show-config] [command]

Cast Flywheel jobs onto --> HPC

positional arguments:
  command          Command to run (run, setup, reset)

options:
  -h, --help       show this help message and exit
  --folder FOLDER  Run, setup, or reset in a specific folder
  --show-config    JSON export: all configs
```

### The fw-hpc-client "command"

The `fw-hpc-client` has three commands

1. `run`: This is the default command and will execute without the need to directly
   specify it.
2. `setup`: This will initialize a configuration directory by populating the
   `./settings` and `./logs` directories. If the `./settings` directory has files, it
   will not overwrite them.
3. `reset`: This command will prompt the user for the removal of the `./settings` and
   `./logs` directories from the configuration directory.

### The `--folder` flag

If the `--folder <Folder>` directive is present, it will execute each "command" within
that directory.

### The `--show-config` flag

If the `--show-config` flag is present and the command is `run`, the configuration will
be printed to the console and exit.
