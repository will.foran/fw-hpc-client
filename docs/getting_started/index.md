# Getting Started

1. Before using the `hpc-client`, you need to decide how it will run on your
   cluster.<br><br>
   [Choose an integration method](1-choose-an-integration-method.md) and keep it in
   mind for later. This sets how frequently the `hpc-client` will look for, pull, and
   queue hpc jobs to your HPC from your Flywheel site.

2. It is strongly recommended that you
   [make a private GitHub repo](2-tracking-changes-privately.md)
   to track your changes.<br><br>
   This will make the `hpc-client` much easier to manage.

3. Perform the [initial cluster setup](3-cluster-install.md). If you are unfamiliar
   with singularity, it is recommended that you read--at a minimum--SingularityCE's
   [introduction](https://sylabs.io/guides/latest/user-guide/introduction.html) and
   [quick start](https://sylabs.io/guides/latest/user-guide/quick_start.html) guides.

4. [Create an authorization token](4-singularity_remote_endpoint.md) so Singularity
   and Flywheel can work with each other.

5. If your queue type is not in the [supported HPC systems table](../resources/index.md#hpc-types), or is sufficiently different, review
   the guide for [adding a queue type](5-development-guide.md).

6. Collaborate with Flywheel staff to
   [install the Flywheel engine in your HPC repo](6-installing_flywheel_engine.md).
   They will also configure the hold engine on your Flywheel site to ensure that other
   engines do not pick up gear jobs that are tagged with "hpc".

7. Complete the integration method you chose in step one.<br><br>
   Confirm the `hpc-client` is running regularly by monitoring `logs/cast.log` and the
   Flywheel user interface.

8. Test and run your first HPC job tests in collaboration with Flywheel. It is
   recommended that you test with MRIQC (non-BIDS version), a gear that's available
   from Flywheel's [Gear Exchange](https://flywheel.io/gear-exchange/).<br><br>
   Note: as of 11 May 2022, Flywheel will have to change the rootfs-url
   (location of where the Docker image resides) for any gears installed from the Gear
   Exchange. For more about how the `hpc-client` uses a rootfs-url, see
   Background/Motivation of
   [this article](https://docs.flywheel.io/admin/compute/admin_using_prebuilt_singularity_images_with_hpc/).

9. Enjoy!
