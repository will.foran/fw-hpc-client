# 2 - Tracking your changes

When using the `hpc-client` in your environment, you may have to modify settings.
Securely saving your settings in a git repository will prevent loss of data and allow
for trackable changes on updating your version of the `hpc-client`.

1. [Create a new GitHub repository](https://github.com/new). Set it to private.

2. Click "Settings" -> "Manage access" -> add any desired collaborators.
In particular, whichever Flywheel staff member(s) you may be working with.

3. Switch to the "Deploy keys" section, then generate a new ssh key on the terminal.
   Leave the passphrase blank:

    ```bash
    ssh-keygen -t ed25519 -C hpc-client-deploy-key -f ./hpc-client-deploy-key

    cat hpc-client-deploy-key.pub
    ```

4. Click "Add deploy key", and paste the public key's contents into the page. You may
   want to tick the box to give it write access also.

5. Copy this key to your HPC for use later:

    ```bash
    scp hpc-client-deploy-key <your-hpc-host>:
    ```

6. Choose a location to store your `hpc-client` credentials:

    ```bash
    cd <configuration directory>
    git clone <The Repository you created above> .
    ```

7. Use this `<configuration directory>` in the
   [Cluster Install](../3-cluster-install/) step.
